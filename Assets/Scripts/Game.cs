﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField]
    GameObject endText;
    [SerializeField]
    LevelProgressUI levelProgressUI;
    [SerializeField]
    GameObject[] levels;
    [SerializeField]
    NumberUI cannonShellsCounterUI;
    [SerializeField]
    Cannon cannon;

    int levelIndex = 0;

    int cannonShellCounter;

    GameObject CurrentLevelRoot;

    int startBodiesCount;
    List<Rigidbody> rigidbodies = new List<Rigidbody>();
    float PercentBodiesCount
    {
        get
        {
            if (startBodiesCount == 0)
                return 1;
            return 1 - (float)rigidbodies.Count / startBodiesCount;
        }
    }

    void Awake()
    {
        cannon.SetLock(true);
        if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {
                Restart();
            });
        }
    }

    void Start()
    {
        cannon.OnShoot += OnCannonShoot;
    }

    public void Restart()
    {
        levelIndex = 0;
        NextLevel();
    }

    void OnCannonShoot()
    {
        SetCannonShell(cannonShellCounter - 1);
        if (cannonShellCounter == 0)
        {
            ShowEndText();
        }
    }

    void SetCannonShell(int value)
    {
        cannonShellCounter = value;
        cannonShellsCounterUI.Set(cannonShellCounter);
    }

    public void NextLevel()
    {
        if (levels.Length > levelIndex)
        {
            endText.SetActive(false);
            cannon.SetLock(false);
            Destroy(CurrentLevelRoot);
            CurrentLevelRoot = Instantiate(levels[levelIndex]);
            levelIndex++;
            LogFBNewLevelEvent("Level " + levelIndex);
            if (levelIndex == levels.Length)
            {
                levelProgressUI.SetFinalLevel(levelIndex);
            }
            else
            {
                levelProgressUI.SetLevel(levelIndex);
            }
            cannon.DestroyBullets();
            SetCannonShell(3 * levelIndex);
            rigidbodies.Clear();
            rigidbodies.AddRange(CurrentLevelRoot.GetComponentsInChildren<Rigidbody>());
            startBodiesCount = rigidbodies.Count;
        }
        else
            ShowEndText();
    }

    void ShowEndText()
    {
        endText.SetActive(true);
        cannon.SetLock(true);
    }

    void OnBodyDown()
    {
        levelProgressUI.SetProgressBar(PercentBodiesCount);
        if (PercentBodiesCount == 1)
            NextLevel();
    }

    public void LogFBNewLevelEvent(string level)
    {
        var param = new Dictionary<string, object>();
        param[AppEventParameterName.Level] = level;
        FB.LogAppEvent(AppEventName.AchievedLevel, 1, param);
    }

    void Update()
    {
        bool bodyDown = false;

        for (int i = 0; i < rigidbodies.Count;)
        {
            if (rigidbodies[i].position.y < -2)
            {
                rigidbodies.RemoveAt(i);
                bodyDown = true;
                continue;
            }
            i++;
        }
        if (bodyDown)
            OnBodyDown();
    }
}
