﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [SerializeField]
    GameObject bulletPrefab;

    [SerializeField]
    GameObject shootPoint;

    public Action OnShoot;

    bool locked;

    List<GameObject> bullets = new List<GameObject>();

    public void SetLock(bool locked)
    {
        this.locked = locked;
    }

    public void DestroyBullets()
    {
        for (int i = 0; i < bullets.Count; i++)
        {
            Destroy(bullets[i]);
        }
        bullets.Clear();
    }

    void Update()
    {
        if (locked)
            return;
        if (Input.GetMouseButtonDown(0) && Input.mousePosition.y > 100)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 1000);
            transform.forward = hit.point + Vector3.up * .5f - transform.position;
            GameObject bullet = Instantiate(bulletPrefab, shootPoint.transform.position, Quaternion.identity);
            bullets.Add(bullet);
            bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 1000);
            OnShoot?.Invoke();
        }
    }
}
