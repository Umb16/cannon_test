﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgerssbarUI : MonoBehaviour
{
    [SerializeField]
    RectTransform barFiller;

    public void SetFill(float value)
    {
        barFiller.localScale = new Vector3(value, 1, 1);
    }
}
