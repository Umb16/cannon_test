﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberUI : MonoBehaviour
{
    [SerializeField]
    Text text;

    public void Set(int value)
    {
        text.text = value.ToString();
    }
}
