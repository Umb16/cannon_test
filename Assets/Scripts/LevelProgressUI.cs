﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelProgressUI : MonoBehaviour
{
    [SerializeField]
    NumberUI currentLevel;
    [SerializeField]
    NumberUI nextLevel;
    [SerializeField]
    ProgerssbarUI progressBar;

    public void SetProgressBar(float value)
    {
        progressBar.SetFill(value);
    }

    public void SetLevel(int value)
    {
        currentLevel.Set(value);
        nextLevel.Set(value+1);
        nextLevel.gameObject.SetActive(true);
        SetProgressBar(0);
    }
    public void SetFinalLevel(int value)
    {
        currentLevel.Set(value);
        nextLevel.gameObject.SetActive(false);
        SetProgressBar(0);
    }
}
